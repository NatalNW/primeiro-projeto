terraform {
  backend "s3" {
    bucket = "base-config-341463"
    key    = "ci-cd"
    region = "us-east-1"
  }
}
